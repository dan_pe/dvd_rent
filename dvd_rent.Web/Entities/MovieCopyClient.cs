﻿using System;

namespace dvd_rent.Web.Models
{
    public class MovieCopyClient
    {
        public int ClientId { get; set; }
        public int MovieCopyId { get; set; }
        public int Id { get; set; }
        public DateTime TakeDate { get; set; }
        public DateTime? BackDate { get; set; }
    }
}